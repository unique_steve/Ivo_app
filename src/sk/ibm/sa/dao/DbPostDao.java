package sk.ibm.sa.dao;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.commons.dbutils.QueryLoader;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import sk.ibm.sa.Address;
import sk.ibm.sa.Post;
import sk.ibm.sa.db.DataSourceFactory;
import sk.ibm.sa.db.PostHandler;
import sk.ibm.sa.db.UserHandler;
import sk.ibm.sa.exceptions.DatabaseException;

public class DbPostDao {
	
	private QueryRunner queryRunner;
	private QueryLoader queryLoader;
	private static final String SQL_FILE = "/sql.xml";

	public DbPostDao() {
		DataSource dataSource = DataSourceFactory.getInstance().getDataSource();
		queryRunner = new QueryRunner(dataSource);
		queryLoader = QueryLoader.instance();
	}

	public void AddPost(Post post) {
		try {			
			String sql = getQuery("addPost");
			queryRunner.insert(sql, new ScalarHandler<>(), post.getUserId(), post.getText());
			
		} catch (SQLException e) {
			throw new DatabaseException("Add post failed", e);
		}
		
	}

	public List<Post> GetAllPosts(){
		String sql = getQuery("selectPosts");
		try {
			return queryRunner.query(sql, new PostHandler());
		} catch (SQLException e) {
			throw new DatabaseException("Select posts failed", e);
		}
	}
	
	public List<Post> GetPostsByUserId(int id){
		String sql = getQuery("getPostsByUserId");
		try {
			return queryRunner.query(sql, new PostHandler(), id);
		} catch (SQLException e) {
			throw new DatabaseException("Select posts failed", e);
		}
	}
	
	public String getQuery(String query) {
		try {
			Map<String, String> queries = queryLoader.load(SQL_FILE);
			return queries.get(query);
		} catch (IOException e) {
			throw new DatabaseException("SQL query loading failed", e);
		}
	}
}
