package sk.ibm.sa.dao;

public class DaoFactory {

	private static final DaoFactory INSTANCE = new DaoFactory();
	
	private UserDao userDao;
	private DbPostDao dbPostDao;
	private DbFriendDao dbFriendListDao;
	
	public static DaoFactory getInstance() {
		return INSTANCE;
	}
	
	private DaoFactory() {
		
	}
	
	public DbFriendDao dbFriendListDao(){
		if (dbFriendListDao == null) {
			dbFriendListDao = new DbFriendDao();
		}
		return dbFriendListDao;
	}
	
	public DbPostDao getPostDao() {
		if (dbPostDao == null) {
			dbPostDao = new DbPostDao();
		}
		return dbPostDao;
	}
	
	public UserDao getUserDao() {
		if (userDao == null) {
			userDao = new DbUserDao();
		}
		return userDao;
	}
	
}
