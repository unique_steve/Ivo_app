package sk.ibm.sa.dao;
import java.util.List;

import sk.ibm.sa.User;

public interface UserDao {

	public void addUser(User user);
	
	public List<User> getAllUsers();
	
	public long getUserCount();

	public void deleteAllUsers();
	
	public User getUserByEmail(String email); 
	
	/**
	 * interface
	 * @param email
	 * @param password - hash
	 * @return
	 */
	public boolean hasUser(String nickName, String password);

	public User getUserById(long id);  
	
	public User getUserByNickName(String nickName);
	
	public boolean checkNick(String nick);
	
	public boolean checkEmail(String email);
	
}
