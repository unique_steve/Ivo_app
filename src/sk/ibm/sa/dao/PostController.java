package sk.ibm.sa.dao;

import java.util.ArrayList;
import java.util.List;

import sk.ibm.sa.Post;

public class PostController {

	private DbPostDao postDao;
	private FriendsController friendListController;
	
	public PostController() {
		postDao = DaoFactory.getInstance().getPostDao();
		friendListController = new FriendsController();
	}

	public void addPost(Post post) {
		postDao.AddPost(post);
		
	}

	public List<Post> getFriendPosts(int id) {
		
		List<Integer> friendsID = friendListController.getFriendsIdList(id);
		friendsID.add(id);
		
		List<Post> friendsPosts = new ArrayList<Post>();
		List<Post> allPosts = postDao.GetAllPosts();
		
		for(Post post : allPosts){
			for(int i : friendsID){
				if(i == post.getUserId())
					friendsPosts.add(post);
			}
		}
		
		return friendsPosts;
	}
	
	public List<Post> getPostByUserId(int id){
		return postDao.GetPostsByUserId(id);
	}

}
