package sk.ibm.sa.dao;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import sk.ibm.sa.Country;
import sk.ibm.sa.User;
import sk.ibm.sa.utils.LoginUtils;

public class UserController {
	
	private UserDao userDao;
	
	public UserController() {
		userDao = DaoFactory.getInstance().getUserDao();
	}

	public void addUser(User user) {
		userDao.addUser(user);	
	}

	/**
	 * 
	 * @param email
	 * @param password - unhashed
	 * @return
	 */
	public boolean hasUser(String nickName, String password) {
		String passHash = LoginUtils.hash(password);
		return userDao.hasUser(nickName, passHash);
	}
	
	public boolean checkNick(String nick){
		return userDao.checkNick(nick);
	}
	
	public boolean checkEmail(String email){
		return userDao.checkNick(email);
	}
	
	
	public User getUserByNickName(String nickName){
		return userDao.getUserByNickName(nickName);
	}

	
	
	public List<User> getAllUsers() {
		return userDao.getAllUsers();
	}
	
	public User getUserById(long id) {
		return userDao.getUserById(id);
	}
	
	
	
}
