package sk.ibm.sa.dao;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.commons.dbutils.QueryLoader;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import sk.ibm.sa.Friends;
import sk.ibm.sa.db.DataSourceFactory;
import sk.ibm.sa.db.FriendsHandler;
import sk.ibm.sa.db.PostHandler;
import sk.ibm.sa.exceptions.DatabaseException;

public class DbFriendDao {

	private QueryRunner queryRunner;
	private QueryLoader queryLoader;
	private static final String SQL_FILE = "/sql.xml";

	public void addFriends(Friends friends){
		try {			
			String sql = getQuery("addFriends");
			queryRunner.insert(sql, new ScalarHandler<>(), friends.getUser_id1(), friends.getUser_id2());
			
		} catch (SQLException e) {
			throw new DatabaseException("Add post failed", e);
		}
	}
	
	
	public DbFriendDao() {
		DataSource dataSource = DataSourceFactory.getInstance().getDataSource();
		queryRunner = new QueryRunner(dataSource);
		queryLoader = QueryLoader.instance();
	}
	
	public List<Friends> getFriendList(int id){
		String sql = getQuery("getFriendList");
		try {
			return queryRunner.query(sql, new FriendsHandler(), id, id);
		} catch (SQLException e) {
			throw new DatabaseException("Select friend list failed", e);
		}
	}
	
	public String getQuery(String query) {
		try {
			Map<String, String> queries = queryLoader.load(SQL_FILE);
			return queries.get(query);
		} catch (IOException e) {
			throw new DatabaseException("SQL query loading failed", e);
		}
	}
}
