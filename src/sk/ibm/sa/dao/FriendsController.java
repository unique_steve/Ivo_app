package sk.ibm.sa.dao;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import sk.ibm.sa.Friends;
import sk.ibm.sa.User;

public class FriendsController {
	
private DbFriendDao friendDao;
private UserController userController = new UserController();
	
	public FriendsController() {
		friendDao = DaoFactory.getInstance().dbFriendListDao();
	}

	public void addFriends(Friends friends){
		friendDao.addFriends(friends);
	}
	
	public List<Integer> getFriendsIdList(int id){
		
		List<Friends> friends = friendDao.getFriendList(id);
		List<Integer> friendsID = new ArrayList<Integer>();
		
		for(Friends friend : friends){
			if(friend.getUser_id1() == id) friendsID.add(friend.getUser_id2());
			else friendsID.add(friend.getUser_id1());
		}
		
		return friendsID;
	}
	
	public List<User> getFriendsList(int id){
		List<User> friends = new ArrayList<User>();
		List<Integer> friendsID = getFriendsIdList(id);
		
		for(int i : friendsID){
			friends.add(userController.getUserById(i));
		}
		
		return friends;
	}
	
	public List<User> getNoFriendsList(int id){
		List<User> allUser = userController.getAllUsers();
		List<Integer> friendsID = getFriendsIdList(id);
		friendsID.add(id);
		
		
		for (Iterator<User> it = allUser.iterator(); it.hasNext(); ) {
			if(allUser.size() == 0)return allUser;
			
		    User user = it.next(); 
		    for(int i : friendsID){
		    	if(i == user.getId_user())
		    	 it.remove();    	
		    }
		}
		
		return allUser;
	}
	
	
}











