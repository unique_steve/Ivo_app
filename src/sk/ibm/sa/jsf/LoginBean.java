package sk.ibm.sa.jsf;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.swing.plaf.synth.SynthSeparatorUI;

import sk.ibm.sa.Country;
import sk.ibm.sa.User;
import sk.ibm.sa.dao.UserController;
import sk.ibm.sa.utils.LoginUtils;

@Named("login")
@SessionScoped
public class LoginBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private UserController userController;

	/**
	 * password - unhashed
	 */
	private String password;
	private String nickName;
	private User user;
	
	public LoginBean() {
		userController = new UserController();
	}

	public Country[] getCountries() {
		return Country.values();
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getNickName() {
		return nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public void loadUser() {
		user = userController.getUserByNickName(nickName);
	}
	
	public String login() {
		if (userController.hasUser(nickName, password)) {
						
			LoginUtils.setHttpSessionEmailAttribute(nickName);
			return "user.xhtml";
		} else {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
					"Incorrect Nick or Password", "Incorrect Nick or Password"));
			return "index.xhtml?faces-redirect=true";
		}
	}

	
	public String logout() {
		LoginUtils.getHttpSession().invalidate();
		return "index.xhtml?faces-redirect=true";
	}

}
