package sk.ibm.sa.jsf;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import sk.ibm.sa.utils.LoginUtils;

@WebFilter(filterName = "loginFilter", urlPatterns = { "*.xhtml" })
public class LoginFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		if (request instanceof HttpServletRequest && response instanceof HttpServletResponse) {
			HttpServletRequest httpRequest = (HttpServletRequest) request;
			HttpServletResponse httpResponse = (HttpServletResponse) response;
			HttpSession session = httpRequest.getSession(false);
			
			if (session != null && session.getAttribute(LoginUtils.NICK_NAME) != null 
					|| httpRequest.getRequestURI().indexOf("index.xhtml") >= 0
					|| httpRequest.getRequestURI().indexOf("public") >= 0
					|| httpRequest.getRequestURI().indexOf("login.xhtml") >= 0
					|| httpRequest.getRequestURI().indexOf("registerContent.xhtml") >= 0
					|| httpRequest.getRequestURI().indexOf("javax.faces.resource") >= 0) {
				chain.doFilter(request, response);
			} else {
				httpResponse.sendRedirect(httpRequest.getContextPath() + "/index.xhtml");
			}
		}
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
	}

	@Override
	public void destroy() {
	}

}
