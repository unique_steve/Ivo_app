package sk.ibm.sa.jsf;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import sk.ibm.sa.Friends;
import sk.ibm.sa.User;
import sk.ibm.sa.dao.FriendsController;

@Named("friends")
@SessionScoped
public class FriendsBean implements Serializable {

	private FriendsController friendController;
	private List<User> friends;
	private List<User> noFriends;
	private Friends friend;
	
	
	
	
	
	public Friends getFriend() {
		return friend;
	}



	public void setFriend(Friends friend) {
		this.friend = friend;
	}



	public List<User> getNoFriends() {
		return noFriends;
	}



	public void setNoFriends(List<User> noFriends) {
		this.noFriends = noFriends;
	}



	public FriendsBean() {
		friendController = new FriendsController();
	}

	

	public List<User> getFriends() {
		return friends;
	}



	public void setFriends(List<User> friends) {
		this.friends = friends;
	}



	@PostConstruct
	public void init() {
		friends = new ArrayList<User>();
		noFriends = new ArrayList<User>();
		friend = new Friends();
	}
	
	public void loadFriendList(String id){
		friends = friendController.getFriendsList(Integer.parseInt(id));
	}
	
	public void loadNoFriendList(String id){
		noFriends = friendController.getNoFriendsList(Integer.parseInt(id));
	}
	
	public String friendsPage(){
		return "friends.xhtml?faces-redirect=true";
	}
	
	public void addFriends(String id1, String id2){
		friend.setUser_id1(Integer.parseInt(id1));
		friend.setUser_id2(Integer.parseInt(id2));
		friendController.addFriends(friend);
	}
}
