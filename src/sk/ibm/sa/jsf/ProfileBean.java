package sk.ibm.sa.jsf;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import sk.ibm.sa.Post;
import sk.ibm.sa.User;
import sk.ibm.sa.dao.PostController;
import sk.ibm.sa.dao.UserController;

@Named("profile")
@RequestScoped
public class ProfileBean implements Serializable {

	private UserController userController;
	private PostController postController;
	private User user;
	private int id;
	private List<Post> posts;
	
	public ProfileBean() {
		userController = new UserController(); 
		postController = new PostController();
	}

	public User getUser() {
		return user;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	
	public List<Post> getPosts() {
		return posts;
	}

	public void setPosts(List<Post> posts) {
		this.posts = posts;
	}

	public void load() {
		user = userController.getUserById(id);
		posts = postController.getPostByUserId(id);
	}
	
	
}
