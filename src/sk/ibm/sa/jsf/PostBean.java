package sk.ibm.sa.jsf;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import sk.ibm.sa.Post;
import sk.ibm.sa.dao.PostController;

@Named("postBean")
@SessionScoped
public class PostBean implements Serializable {

	public PostBean() {
		// TODO Auto-generated constructor stub
	}
	
	private PostController postController = new PostController();
	private Post post;
	private List<Post> posts;

	public Post getPost() {
		return post;
	}

	public void setPost(Post post) {
		this.post = post;
	}
	
	public List<Post> getPosts() {
		return posts;
	}

	public void setPostList(List<Post> posts) {
		this.posts = posts;
	}

	@PostConstruct
	public void init() {
		post = new Post();
		posts = new ArrayList<Post>();
	}
	
	public void addPost(String id){
		post.setUserId(Integer.parseInt(id));
		postController.addPost(post);
	}
	
	public void loadFriendPosts(String id){
		posts = postController.getFriendPosts(Integer.parseInt(id));
	}
}
