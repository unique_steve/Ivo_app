package sk.ibm.sa.jsf;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import sk.ibm.sa.User;
import sk.ibm.sa.dao.UserController;
import sk.ibm.sa.utils.LoginUtils;

@Named("reg")
@RequestScoped
public class RegistrationBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private UserController userController = new UserController();

	private User user;

	public RegistrationBean() {

	}

	@PostConstruct
	public void init() {
		user = new User();
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String register() {
		if (!LoginUtils.isEmailValid(user.getEmail())) {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Incorrect email format", ""));
			return "registerContent.xhtml?faces-redirect=true";
			
		}
			
		if (userController.checkEmail(user.getEmail())) {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Email already exists", ""));
			return "registerContent.xhtml?faces-redirect=true";
		} 

		if (userController.checkNick(user.getNickName())) {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Nick already exists", ""));
			return "registerContent.xhtml?faces-redirect=true";
		}
		
		userController.addUser(user);
		return "index.xhtml?faces-redirect=true";
	}
}
