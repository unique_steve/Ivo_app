package sk.ibm.sa.jsf;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import sk.ibm.sa.User;
import sk.ibm.sa.dao.UserController;

@Named("userList")
@RequestScoped
public class UserListBean implements Serializable{

	private UserController userController;
	private List<User> users;

	public UserListBean() {
		userController = new UserController();
	}

	@PostConstruct
	public void init() {
		users = userController.getAllUsers();
	}

	public List<User> getUsers() {
		return users;
	}

}
