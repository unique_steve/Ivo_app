package sk.ibm.sa.utils;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

public class LoginUtils {

	public static final Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$",
			Pattern.CASE_INSENSITIVE);

	public static final String NICK_NAME = "nickName";

	public static void setHttpSessionEmailAttribute(String nickName) {
		getHttpSession().setAttribute(NICK_NAME, nickName);
	}

	/**
	 * 
	 * @return String email if user is logged, otherwise return null
	 */
	public static String getHttpSessionEmailAttribute() {
		HttpSession httpSession = getHttpSession();
		if (httpSession == null) {
			return null;
		} else {
			return (String) httpSession.getAttribute(NICK_NAME);
		}
	}

	public static boolean isEmailValid(String email) {
		Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(email);
		return matcher.find();
	}

	public static String hash(String s) {
		try {
			MessageDigest md = MessageDigest.getInstance("md5");
			md.reset();
			md.update(s.getBytes());
			byte[] digest = md.digest();
			BigInteger bigInt = new BigInteger(1, digest);
			return bigInt.toString(16);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static HttpSession getHttpSession() {
		return (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
	}

}
