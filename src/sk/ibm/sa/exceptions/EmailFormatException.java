package sk.ibm.sa.exceptions;

public class EmailFormatException extends Exception{

	public EmailFormatException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public EmailFormatException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public EmailFormatException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public EmailFormatException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public EmailFormatException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	
	
}
