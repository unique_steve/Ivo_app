package sk.ibm.sa;

import java.io.Serializable;

public class Post implements Serializable{

	private int id_post;
	private int userId;
	private String text;
	private User user;
	private String date;

	
	
	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Post() {
		// TODO Auto-generated constructor stub
	}

	public Post(String text, User user, String date) {
		super();
		this.text = text;
		this.user = user;
		this.date = date;
	}

	public int getId_post() {
		return id_post;
	}

	public void setId_post(int id_post) {
		this.id_post = id_post;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

}
