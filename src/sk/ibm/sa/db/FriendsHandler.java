package sk.ibm.sa.db;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.commons.dbutils.handlers.AbstractListHandler;

import sk.ibm.sa.Friends;

public class FriendsHandler extends AbstractListHandler<Friends>{

	@Override
	protected Friends handleRow(ResultSet rs) throws SQLException {
		
		Friends friends = new Friends(rs.getInt("id_user1"), rs.getInt("id_user2"));
		
		return friends;
	}

}
