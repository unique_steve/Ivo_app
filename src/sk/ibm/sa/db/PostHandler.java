package sk.ibm.sa.db;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.commons.dbutils.handlers.AbstractListHandler;

import sk.ibm.sa.Address;
import sk.ibm.sa.Country;
import sk.ibm.sa.Post;
import sk.ibm.sa.User;
import sk.ibm.sa.exceptions.EmailFormatException;

public class PostHandler extends AbstractListHandler<Post>{

	@Override
	protected Post handleRow(ResultSet rs) throws SQLException {
		try {			
			User user = new User(rs.getString("name"), rs.getString("surname"), 
					null, rs.getString("nickName"), rs.getString("email"), rs.getString("password"));
			
			user.setId_user(rs.getInt("id_user"));
			
			Post post = new Post(rs.getString("text"), user, rs.getString("datetime").substring(0, 19));
			post.setUserId(rs.getInt("id_user"));
			return post;
			
		} catch (EmailFormatException e) {
			e.printStackTrace();
		}
		return null;
	}

}
