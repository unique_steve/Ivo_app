package sk.ibm.sa;

import java.io.Serializable;

public class Friends  implements Serializable {
	
	public Friends(){
		
	}
	
	private int user_id1;
	private int user_id2;
	
	public Friends(int user_id1, int user_id2) {
		super();
		this.user_id1 = user_id1;
		this.user_id2 = user_id2;
	}
	public int getUser_id1() {
		return user_id1;
	}
	public void setUser_id1(int user_id1) {
		this.user_id1 = user_id1;
	}
	public int getUser_id2() {
		return user_id2;
	}
	public void setUser_id2(int user_id2) {
		this.user_id2 = user_id2;
	}
}
